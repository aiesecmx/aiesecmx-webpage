SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `aiesecmx_recriut` ;
CREATE SCHEMA IF NOT EXISTS `aiesecmx_recriut` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `aiesecmx_recriut` ;

-- -----------------------------------------------------
-- Table `aiesecmx_recriut`.`Languages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `aiesecmx_recriut`.`Languages` ;

CREATE  TABLE IF NOT EXISTS `aiesecmx_recriut`.`Languages` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aiesecmx_recriut`.`Countries`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `aiesecmx_recriut`.`Countries` ;

CREATE  TABLE IF NOT EXISTS `aiesecmx_recriut`.`Countries` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aiesecmx_recriut`.`States`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `aiesecmx_recriut`.`States` ;

CREATE  TABLE IF NOT EXISTS `aiesecmx_recriut`.`States` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `countryId` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_CountryId_idx` (`countryId` ASC) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) ,
  CONSTRAINT `FK_CountryId_States`
    FOREIGN KEY (`countryId` )
    REFERENCES `aiesecmx_recriut`.`Countries` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aiesecmx_recriut`.`Cities`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `aiesecmx_recriut`.`Cities` ;

CREATE  TABLE IF NOT EXISTS `aiesecmx_recriut`.`Cities` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `stateId` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_StateId_idx` (`stateId` ASC) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) ,
  CONSTRAINT `FK_StateId_Cities`
    FOREIGN KEY (`stateId` )
    REFERENCES `aiesecmx_recriut`.`States` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aiesecmx_recriut`.`Users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `aiesecmx_recriut`.`Users` ;

CREATE  TABLE IF NOT EXISTS `aiesecmx_recriut`.`Users` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `lastName` VARCHAR(45) NOT NULL ,
  `eMail` VARCHAR(45) NOT NULL ,
  `birthday` DATE NOT NULL ,
  `graduationDate` DATE NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `eMail_UNIQUE` (`eMail` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aiesecmx_recriut`.`StudyAreas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `aiesecmx_recriut`.`StudyAreas` ;

CREATE  TABLE IF NOT EXISTS `aiesecmx_recriut`.`StudyAreas` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aiesecmx_recriut`.`LanguageLevels`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `aiesecmx_recriut`.`LanguageLevels` ;

CREATE  TABLE IF NOT EXISTS `aiesecmx_recriut`.`LanguageLevels` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aiesecmx_recriut`.`languagesSpoken`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `aiesecmx_recriut`.`languagesSpoken` ;

CREATE  TABLE IF NOT EXISTS `aiesecmx_recriut`.`languagesSpoken` (
  `userId` INT NOT NULL ,
  `languageId` INT NOT NULL ,
  `levelId` INT NOT NULL ,
  INDEX `FK_User_idx` (`userId` ASC) ,
  INDEX `FK_LanguageId_idx` (`languageId` ASC) ,
  PRIMARY KEY (`languageId`, `userId`) ,
  INDEX `FK_LevelId_idx` (`levelId` ASC) ,
  CONSTRAINT `FK_UserId_LanguagesSpoken`
    FOREIGN KEY (`userId` )
    REFERENCES `aiesecmx_recriut`.`Users` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `FK_LanguageId_LanguagesSpoken`
    FOREIGN KEY (`languageId` )
    REFERENCES `aiesecmx_recriut`.`Languages` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `FK_LevelId_LanguagesSpoken`
    FOREIGN KEY (`levelId` )
    REFERENCES `aiesecmx_recriut`.`LanguageLevels` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aiesecmx_recriut`.`Universities`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `aiesecmx_recriut`.`Universities` ;

CREATE  TABLE IF NOT EXISTS `aiesecmx_recriut`.`Universities` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `cityId` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) ,
  INDEX `FK_CityId_idx` (`cityId` ASC) ,
  CONSTRAINT `FK_CityId_Universities`
    FOREIGN KEY (`cityId` )
    REFERENCES `aiesecmx_recriut`.`Cities` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aiesecmx_recriut`.`UserOrigin`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `aiesecmx_recriut`.`UserOrigin` ;

CREATE  TABLE IF NOT EXISTS `aiesecmx_recriut`.`UserOrigin` (
  `userId` INT NOT NULL ,
  `cityId` INT NOT NULL ,
  `universityId` INT NOT NULL ,
  `studyAreaId` INT NOT NULL ,
  PRIMARY KEY (`userId`, `cityId`) ,
  INDEX `FK_UserId_idx` (`userId` ASC) ,
  INDEX `FK_CityId_idx` (`cityId` ASC) ,
  INDEX `FK_UniversityId_idx` (`universityId` ASC) ,
  INDEX `FK_StudyAreaId_idx` (`studyAreaId` ASC) ,
  CONSTRAINT `FK_UserId_UserOrigin`
    FOREIGN KEY (`userId` )
    REFERENCES `aiesecmx_recriut`.`Users` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `FK_CityId_UserOrigin`
    FOREIGN KEY (`cityId` )
    REFERENCES `aiesecmx_recriut`.`Cities` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `FK_UniversityId_UserOrigin`
    FOREIGN KEY (`universityId` )
    REFERENCES `aiesecmx_recriut`.`Universities` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `FK_StudyAreaId_UserOrigin`
    FOREIGN KEY (`studyAreaId` )
    REFERENCES `aiesecmx_recriut`.`StudyAreas` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aiesecmx_recriut`.`Regions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `aiesecmx_recriut`.`Regions` ;

CREATE  TABLE IF NOT EXISTS `aiesecmx_recriut`.`Regions` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aiesecmx_recriut`.`RegistrationTypes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `aiesecmx_recriut`.`RegistrationTypes` ;

CREATE  TABLE IF NOT EXISTS `aiesecmx_recriut`.`RegistrationTypes` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aiesecmx_recriut`.`Registrations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `aiesecmx_recriut`.`Registrations` ;

CREATE  TABLE IF NOT EXISTS `aiesecmx_recriut`.`Registrations` (
  `userId` INT NOT NULL ,
  `reason` VARCHAR(45) NOT NULL ,
  `heardAbout` VARCHAR(45) NOT NULL ,
  `regionId` INT NOT NULL ,
  `registrationTypeId` INT NOT NULL ,
  PRIMARY KEY (`userId`, `registrationTypeId`) ,
  INDEX `FK_UserId_idx` (`userId` ASC) ,
  INDEX `FK_RegionId_idx` (`regionId` ASC) ,
  INDEX `FK_InternshipTypeId_idx` (`registrationTypeId` ASC) ,
  CONSTRAINT `FK_UserId_Registrations`
    FOREIGN KEY (`userId` )
    REFERENCES `aiesecmx_recriut`.`Users` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `FK_RegionId_Registrations`
    FOREIGN KEY (`regionId` )
    REFERENCES `aiesecmx_recriut`.`Regions` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `FK_RegistrationTypeId_Registrations`
    FOREIGN KEY (`registrationTypeId` )
    REFERENCES `aiesecmx_recriut`.`RegistrationTypes` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
