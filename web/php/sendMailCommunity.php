<?php
	$state = $_POST["state"];
	$ToEmail = 'communications@aiesec.org.mx,webmaster@aiesec.org.mx';
	
	switch ($state) {
		case "Aguascalientes":
			$ToEmail .= ',recruitment.ags@aiesec.org.mx';
			$ToEmail .= ',outgoing.ags@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Baja California":
			$ToEmail .= ',aiesec.ml@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Baja California Sur":
			$ToEmail .= ',aiesec.ml@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Campeche":
			$ToEmail .= ',recruitment.tb@aiesec.org.mx';
			$ToEmail .= ',outgoing.tb@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Chiapas":
			$ToEmail .= ',recruitment.tb@aiesec.org.mx';
			$ToEmail .= ',outgoing.tb@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Chihuahua":
			$ToEmail .= ',recruitment.cj@aiesec.org.mx';
			$ToEmail .= ',outgoing.cj@aiesec.org.mx';
			$ToEmail .= ',recruitment.ch@aiesec.org.mx';
			$ToEmail .= ',out.social.ch@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Coahuila":
			$ToEmail .= ',aiesec.mo@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Colima":
			$ToEmail .= ',recruitment.io@aiesec.org.mx';
			$ToEmail .= ',outgoing.io@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Distrito Federal":
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Durango":
			$ToEmail .= ',recruitment.io@aiesec.org.mx';
			$ToEmail .= ',outgoing.io@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Guanajuato":
			$ToEmail .= ',recruitment.gt@aiesec.org.mx';
			$ToEmail .= ',out.social.gt@aiesec.org.mx';
			$ToEmail .= ',recruitment.ln@aiesec.org.mx';
			$ToEmail .= ',outgoing.ln@aiesec.org.mx';
				
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Guerrero":
			$ToEmail .= ',recruitment.io@aiesec.org.mx';
			$ToEmail .= ',outgoing.io@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Hidalgo":
			$ToEmail .= ',recruitment.qro@aiesec.org.mx';
			$ToEmail .= ',out.social.qro@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Jalisco":
			$ToEmail .= ',recruitment.io@aiesec.org.mx';
			$ToEmail .= ',outgoing.io@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "México":
			$ToEmail .= ',recruitment.um@aiesec.org.mx';
			$ToEmail .= ',outgoing.um@aiesec.org.mx';
				
			$ToEmail .= ',recruitment.em@aiesec.org.mx';
			$ToEmail .= ',outgoing.global.em@aiesec.org.mx';
				
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Michoacán":
			$ToEmail .= ',recruitment.io@aiesec.org.mx';
			$ToEmail .= ',outgoing.io@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Morelos":
			$ToEmail .= ',recruitment.cv@aiesec.org.mx';
			$ToEmail .= ',outgoing.cv@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Nayarit":
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Nuevo León":
			$ToEmail .= ',recruitment.is@aiesec.org.mx';
			$ToEmail .= ',outgoing.is@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Oaxaca":
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Puebla":
			$ToEmail .= ',recruitment.ud@aiesec.org.mx';
			$ToEmail .= ',outgoing.ud@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Querétaro":
			$ToEmail .= ',recruitment.qro@aiesec.org.mx';
			$ToEmail .= ',out.social.qro@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Quintana Roo":
			$ToEmail .= ',recruitment.me@aiesec.org.mx';
			$ToEmail .= ',outgoing.me@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "San Luis Potosí":
			$ToEmail .= ',recruitment.gt@aiesec.org.mx';
			$ToEmail .= ',out.social.gt@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Sinaloa":
			$ToEmail .= ',recruitment.cu@aiesec.org.mx';
			$ToEmail .= ',out.social.cu@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Sonora":
			$ToEmail .= ',recruitment.cj@aiesec.org.mx';
			$ToEmail .= ',outgoing.cj@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Tabasco":
			$ToEmail .= ',recruitment.tb@aiesec.org.mx';
			$ToEmail .= ',outgoing.tb@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Tamaulipas":
			$ToEmail .= ',recruitment.is@aiesec.org.mx';
			$ToEmail .= ',outgoing.is@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Tlaxcala":
			$ToEmail .= ',recruitment.ud@aiesec.org.mx';
			$ToEmail .= ',outgoing.ud@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Veracruz":
			$ToEmail .= ',recruitment.ve@aiesec.org.mx';
			$ToEmail .= ',in.social.ve@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Yucatán":
			$ToEmail .= ',recruitment.me@aiesec.org.mx';
			$ToEmail .= ',outgoing.me@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
		case "Zacatecas":
			$ToEmail .= ',recruitment.ags@aiesec.org.mx';
			$ToEmail .= ',outgoing.ags@aiesec.org.mx';
			$ToEmail .= ',info@aiesec.org.mx';
			break;
	}
	$EmailSubject = 'GCDP Registration';
	$mailheader = "From: ".$_POST["email"]."\r\n";
	$mailheader .= "Reply-To: ".$_POST["roman.vazquez@vitasoft.com.mx"]."\r\n";
	$mailheader .= "Content-type: text/html; charset=iso-8859-1\r\n";
	$MESSAGE_BODY = "INTERCAMBIO: GCDP"."<br/>";
	$MESSAGE_BODY .= "Name: ".$_POST["name"]." ".$_POST["lastName"]."<br/>";
	$MESSAGE_BODY .= "Email: ".$_POST["email"]."<br/>";
	$MESSAGE_BODY .= "Bday: ".$_POST["bday"]."<br/>";
	$MESSAGE_BODY .= "State: ".$_POST["state"]."<br/>";
	$MESSAGE_BODY .= "University: ".$_POST["university"]."<br/>";
	$MESSAGE_BODY .= "Studio Area: ".$_POST["studyArea"]."<br/>";
	$MESSAGE_BODY .= "Graduation: ".$_POST["graduation"]."<br/>";
	
	$arrLanguages = explode("\n", $_POST["languages"]);
	foreach ( $arrLanguages as $language ) {
		$MESSAGE_BODY .= "Languages: ".$language."<br/>";
	}
	
	$MESSAGE_BODY .= "Why internship: ".$_POST["why"]."<br/>";
	$MESSAGE_BODY .= "Where did hear about AIESEC: ".$_POST["where"]."<br/>";
	mail($ToEmail, $EmailSubject, $MESSAGE_BODY, $mailheader) or die ("Failure");
?>